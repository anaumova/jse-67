package ru.tsc.anaumova.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.anaumova.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.tsc.anaumova.tm.repository")
public class ApplicationConfiguration {

    @Bean
    public DataSource dataSource(
            @NotNull @Value("#{environment['database.driver']}") final String databaseDriver,
            @NotNull @Value("#{environment['database.url']}") final String databaseUrl,
            @NotNull @Value("#{environment['database.username']}") final String databaseUser,
            @NotNull @Value("#{environment['database.password']}") final String databaseUserPassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databaseUserPassword);
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        @Nullable final EntityManagerFactory factory = entityManagerFactory.getObject();
        transactionManager.setEntityManagerFactory(factory);
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @NotNull @Value("#{environment['database.dialect']}") final String databaseDialect,
            @NotNull @Value("#{environment['database.hbm2ddl_auto']}") final String databaseHbm2ddlAuto,
            @NotNull @Value("#{environment['database.show_sql']}") final String databaseShowSql,
            @NotNull @Value("#{environment['cache.level']}") final String useSecondLevelCache,
            @NotNull @Value("#{environment['cache.query']}") final String useQueryCache,
            @NotNull @Value("#{environment['cache.minimal-puts']}") final String useMinimalPuts,
            @NotNull @Value("#{environment['cache.prefix']}") final String cacheRegionPrefix,
            @NotNull @Value("#{environment['cache.config']}") final String cacheProviderConfig,
            @NotNull @Value("#{environment['cache.factory']}") final String cacheRegionFactory
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.anaumova.tm.model", "ru.tsc.anaumova.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseDialect);
        properties.put(Environment.HBM2DDL_AUTO, databaseHbm2ddlAuto);
        properties.put(Environment.SHOW_SQL, databaseShowSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, useSecondLevelCache);
        properties.put(Environment.USE_QUERY_CACHE, useQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, useMinimalPuts);
        properties.put(Environment.CACHE_REGION_PREFIX, cacheRegionPrefix);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, cacheProviderConfig);
        properties.put(Environment.CACHE_REGION_FACTORY, cacheRegionFactory);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}