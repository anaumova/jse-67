package ru.tsc.anaumova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.dto.model.UserDto;
import ru.tsc.anaumova.tm.dto.request.UserProfileRequest;
import ru.tsc.anaumova.tm.dto.response.UserProfileResponse;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.event.ConsoleEvent;
import ru.tsc.anaumova.tm.exception.entity.UserNotFoundException;

@Component
public final class UserShowProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userShowProfileListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull UserProfileResponse response = getUserEndpoint().showProfileUser(new UserProfileRequest(getToken()));
        @Nullable final UserDto user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}