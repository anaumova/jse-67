package ru.tsc.anaumova.tm.listener.data;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.anaumova.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractDataListener extends AbstractListener {

    @Autowired
    private IDomainEndpoint domainEndpoint;

}